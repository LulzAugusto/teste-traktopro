(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('ResourceHelper', ResourceHelper);

    ResourceHelper.$inject = ['API_URL', 'CurrentUser'];

    /* @ngInject */
    function ResourceHelper(API_URL, CurrentUser) {
        var service = {
            buildUrl: buildUrl,
            successResponse: successResponse
        };
        return service;

        function buildUrl(uri) {
            var url = API_URL + '/' + uri;
            if (needsToken(uri)) {
                return url + '/' + CurrentUser.token;
            }
            return url;
        }

        function successResponse(response) {
            return response.data;
        }

        function needsToken(uri) {
            return uri !== 'login';
        }
    }
})();
