(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('AuthResource', AuthResource);

    AuthResource.$inject = ['$http', 'ResourceHelper'];

    /* @ngInject */
    function AuthResource($http, ResourceHelper) {
        var uri = 'login';

        var service = {
            login: login
        };
        return service;

        function login(credentials) {
            return $http
                .post(ResourceHelper.buildUrl(uri), buildBody(credentials))
                .then(ResourceHelper.successResponse);
        }

        function buildBody(credentials) {
            return {
                user: credentials
            };
        }
    }
})();
