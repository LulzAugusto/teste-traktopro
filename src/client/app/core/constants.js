(function() {
    'use strict';

    angular
        .module('app.core')
        .constant('API_URL', 'http://stage.traktopro.com.br/api/v2');
})();
