(function() {
    'use strict';

    angular
        .module('app.core')
        .config(config);

    config.$inject = ['$locationProvider'];

    /* @ngInject */
    function config($locationProvider) {
        $locationProvider.html5Mode(true);
    }
})();
