(function() {
    'use strict';

    angular
        .module('app.core')
        .run(run);

    run.$inject = ['AuthResource', 'CurrentUser', '$state'];

    /* @ngInject */
    function run(AuthResource, CurrentUser, $state) {
        AuthResource.login({email: 'luiz66@gmail.com', password: '123456789'})
            .then(function(data) {
                CurrentUser.token = data.token;
                $state.go('quote');
            });
    }
})();
