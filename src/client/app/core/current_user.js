(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('CurrentUser', CurrentUser);

    /* @ngInject */
    function CurrentUser() {
        var description = {
            enumerable: true,
            writable: true
        };

        var service = {
            build: build
        };

        Object.defineProperty(service, 'token', description);

        return service;

        function build(user) {
            angular.extend(service, user);
        }
    }
})();
