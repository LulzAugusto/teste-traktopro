(function() {
    'use strict';

    angular
        .module('app.layout')
        .controller('ShellController', ShellController);

    ShellController.$inject = ['CurrentUser'];

    /* @ngInject */
    function ShellController(CurrentUser) {
        /*jshint validthis: true */
        var vm = this;

        vm.currentUser = CurrentUser;

        activate();

        function activate() {
        }
    }
})();
