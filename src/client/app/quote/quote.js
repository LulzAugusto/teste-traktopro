(function() {
    'use strict';

    angular
        .module('app.quote')
        .factory('Quote', Quote);

    /* @ngInject */
    function Quote() {
        var _items = [],
            _title = null,
            _client = {};

        var service = {
            addItem: addItem,
            removeItem: removeItem,
            addPackage: addPackage
        };
        Object.defineProperties(service, {
            items: {
                get: function() { return _items; }
            },
            title: {
                get: function() { return _title; },
                set: function(value) { _title = value; }
            },
            subtotal: {
                get: function() {
                    var sum = 0;
                    angular.forEach(_items, function(item) {
                        if (item.isPackage) {
                            angular.forEach(item.items, function(subitem) {
                                if (subitem.active) {
                                    sum += subitem.total;
                                }
                            });
                        } else {
                            sum += item.total;
                        }
                    });
                    return sum;
                }
            },
            total: {
                get: function() {
                    return this.subtotal;
                }
            },
            client: {
                get: function() { return _client; }
            }
        });
        return service;

        function addItem() {
            _items.push(angular.extend(new Item()));
        }

        function addPackage() {
            var itemPackage = new Item();
            itemPackage.items = [new Item()];
            itemPackage.items[0].title = 'Item do pacote';
            itemPackage.items[0].quantity = 2;
            itemPackage.items[0].rate = 5000;
            itemPackage.items[0].unity = 'Person';
            itemPackage.title = 'Pacote';
            _items.push(itemPackage);
        }

        function removeItem(index) {
            _items.splice(index, 1);
        }

        function Item() {
            this.title = 'Title';
            this.quantity = 1;
            this.unit = '-';
            this.rate = 0;
            this.items = [];
            this.active = true;

            Object.defineProperties(this, {
                total: {
                    get: function() { return this.quantity * this.rate; }
                },
                isPackage: {
                    get: function() { return this.items.length > 0; }
                }
            });
        }
    }
})();
