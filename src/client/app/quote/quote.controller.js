(function() {
    'use strict';

    angular
        .module('app.quote')
        .controller('QuoteController', QuoteController);

    QuoteController.$inject = ['Quote', 'QuoteResource'];

    /* @ngInject */
    function QuoteController(Quote, QuoteResource) {
        /*jshint validthis: true */
        var vm = this;

        vm.addItem = addItem;
        vm.create = create;
        vm.itemHover = null;
        vm.model = Quote;
        vm.removeItem = removeItem;
        vm.units = [
            '-', 'Item', 'Person', 'Hour',
            'Minute', 'Second', 'm²', 'Km',
            'Page', 'Lauda'
        ];

        activate();

        function activate() {
            Quote.addItem();
            Quote.addPackage();
        }

        function addItem() {
            Quote.addItem();
        }

        function removeItem(index) {
            Quote.removeItem(index);
        }

        function create() {
            return QuoteResource.create(Quote)
                .then(function(data) {
                    console.log('Orcamento criado.\nId: %s \nData de criação: %s \nObjeto: %o',
                        data.id, data.created_at, data);
                });
        }
    }
})();
