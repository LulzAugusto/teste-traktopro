(function() {
    'use strict';

    angular
        .module('app.quote')
        .factory('QuoteResource', QuoteResource);

    QuoteResource.$inject = ['$http', 'ResourceHelper'];

    /* @ngInject */
    function QuoteResource($http, ResourceHelper) {
        var uri = 'projects';

        var service = {
            create: create
        };
        return service;

        function create(quote) {
            return $http
                .post(ResourceHelper.buildUrl(uri), buildBody(quote))
                .then(ResourceHelper.successResponse);
        }

        function buildBody(quote) {
            return {
                client: quote.client,
                services: quote.items,
                title: quote.title
            };
        }
    }
})();
