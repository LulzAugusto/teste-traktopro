(function() {
    'use strict';

    angular
        .module('app.quote')
        .config(config);

    config.$inject = ['$stateProvider'];

    /* @ngInject */
    function config($stateProvider) {
        $stateProvider
            .state('quote', {
                url: '/',
                templateUrl: 'app/quote/quote.html',
                controller: 'QuoteController',
                controllerAs: 'quote'
            });
    }
})();
